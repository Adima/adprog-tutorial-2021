package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.List;

public class ChainSpell implements Spell {
    private List<Spell> spells;

    public ChainSpell(List<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        Spell lastSpell = spells.get(spells.size() - 1);
        spells.set((spells.size() - 1), spells.get(spells.size() - 2));
        spells.set((spells.size() - 2), lastSpell);
        spells.get(spells.size() - 1).cast();
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
