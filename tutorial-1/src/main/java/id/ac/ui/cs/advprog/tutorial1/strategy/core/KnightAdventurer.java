package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    @Override
    public String getAlias() {
        return "Crusader";
    }

    public KnightAdventurer() {
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }
}
