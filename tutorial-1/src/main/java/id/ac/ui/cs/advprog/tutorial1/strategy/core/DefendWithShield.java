package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

    @Override
    public String getType() {
        return "Kite_Shield";
    }

    @Override
    public String defend() {
        return "blocks";
    }
}
