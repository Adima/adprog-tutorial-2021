package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    @Override
    public String getAlias() {
        return "Magician";
    }

    public MysticAdventurer() {
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }
}
