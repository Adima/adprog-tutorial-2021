package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    @Override
    public String getType() {
        return "Mac-10";
    }

    @Override
    public String attack() {
        return "brrrrraaaa";
    }
}
