package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
        this.guild.add(this);
    }

    @Override
    public void update() {
        getQuests().add(guild.getQuest());
    }
}
