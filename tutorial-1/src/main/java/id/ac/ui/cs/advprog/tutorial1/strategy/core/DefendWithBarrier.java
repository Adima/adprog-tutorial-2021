package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {

    @Override
    public String getType() {
        return "Energy_Barrier";
    }

    @Override
    public String defend() {
        return "zoom";
    }
}
